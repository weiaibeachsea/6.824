package mr

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"sync"
	"time"

	. "6.824/common"
)

const timeTolerance = 10 * time.Second

type Coordinator struct {
	// Your definitions here.
	toMap         []TaskInfo
	mapping       []TaskInfo
	toReduce      []TaskInfo
	reducing      []TaskInfo
	toMapIndex    int
	mappingIndex  int
	toReduceIndex int
	reducingIndex int
	nReduce       int
	nFiles        int
	mapDone       bool
	mutex         sync.Mutex
}

// Your code here -- RPC handlers for the worker to call.
func (c *Coordinator) AskMission(args *ExampleArgs, reply *TaskInfo) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	// reply.Y = args.X + 1
	if len(c.toMap) > 0 {
		// c.toMapIndex--
		*reply = c.toMap[len(c.toMap)-1]
		c.toMap = c.toMap[:len(c.toMap)-1]
		reply.BeginTime = time.Now()
		c.mapping = append(c.mapping, *reply)
		// c.mappingIndex++
		return nil
	} else if len(c.mapping) != 0 {
		reply.State = WaitState
		return nil
	} else if !c.mapDone {
		for i := 0; i < c.nReduce; i++ {
			c.toReduce = append(c.toReduce, TaskInfo{
				State:     ReduceState,
				FileName:  "",
				FileIndex: 0,
				PartIndex: i,
				NReduce:   c.nReduce,
				NFiles:    c.nFiles,
			})
			// c.toReduceIndex++
		}
		reply.State = WaitState
		c.mapDone = true
		return nil
	} else if len(c.toReduce) > 0 {
		// c.toReduceIndex--
		*reply = c.toReduce[len(c.toReduce)-1]
		c.toReduce = c.toReduce[:len(c.toReduce)-1]
		reply.BeginTime = time.Now()
		c.reducing = append(c.reducing, *reply)
		// c.reducingIndex++
		return nil
	} else if len(c.reducing) != 0 {
		reply.State = WaitState
		return nil
	} else {
		// c.end = true
		reply.State = EndState
		return nil
	}

	// return nil
}

func (c *Coordinator) DoneMission(args *TaskInfo, reply *TaskInfo) error {
	c.mutex.Lock()
	fmt.Printf("%+v\n", args)
	switch args.State {
	case MapState:
		{
			for i, v := range c.mapping {
				if v.FileIndex == args.FileIndex {
					c.mapping = append(c.mapping[:i], c.mapping[i+1:]...)
					// c.mappingIndex--
					break
				}
			}
			args.State = ReduceState
			// c.toReduce = append(c.toReduce)
			break
		}
	case ReduceState:
		{
			for i, v := range c.reducing {
				if v.PartIndex == args.PartIndex {
					c.reducing = append(c.reducing[:i], c.reducing[i+1:]...)
					// c.reducingIndex--
					break
				}
			}
			// args.State = ReduceState
			// c.toReduce = append(c.toReduce)
			break
		}
	}
	c.mutex.Unlock()
	return nil
}

//
// an example RPC handler.
//
// the RPC argument and reply types are defined in rpc.go.
//
func (c *Coordinator) Example(args *ExampleArgs, reply *ExampleReply) error {
	reply.Y = args.X + 1
	return nil
}

//
// start a thread that listens for RPCs from worker.go
//
func (c *Coordinator) server() {
	rpc.Register(c)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := coordinatorSock()
	os.Remove(sockname)
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

//
// main/mrcoordinator.go calls Done() periodically to find out
// if the entire job has finished.
//
func (c *Coordinator) Done() bool {
	ret := false

	// Your code here.
	c.mutex.Lock()
	if c.mapDone && len(c.reducing) == 0 && len(c.toReduce) == 0 {
		ret = true
		log.Println("asked whether i am done, replying yes...")
	}
	log.Println(c.mapDone, len(c.reducing), len(c.toReduce))
	c.mutex.Unlock()
	log.Println("asked whether i am done, replying no...")
	return ret
}

//
// create a Coordinator.
// main/mrcoordinator.go calls this function.
// nReduce is the number of reduce tasks to use.
//
func MakeCoordinator(files []string, nReduce int) *Coordinator {
	c := Coordinator{}
	c.nReduce = nReduce
	c.nFiles = len(files)
	// Your code here.
	for k, v := range files {
		c.toMap = append(c.toMap, TaskInfo{
			State:     MapState,
			FileName:  v,
			FileIndex: k,
			PartIndex: 0,
			NReduce:   nReduce,
			NFiles:    len(files),
		})
		fmt.Println(v)
		// c.toMapIndex++
	}
	c.server()
	go checkTimeout(&c)
	return &c
}

func checkTimeout(c *Coordinator) {
	for {
		fmt.Println("check")
		c.mutex.Lock()
		for i := 0; i < len(c.mapping); i++ {
			if time.Now().Sub(c.mapping[i].BeginTime) > timeTolerance {
				// task:=c.mapping[i]
				// task.BeginTime=time.Now()
				c.toMap = append(c.toMap, c.mapping[i])
				c.mapping = append(c.mapping[:i], c.mapping[i+1:]...)
				i--
			}
		}
		for i := 0; i < len(c.reducing); i++ {
			if time.Now().Sub(c.reducing[i].BeginTime) > timeTolerance {
				// task:=c.reducing[i]
				// task.BeginTime=time.Now()
				c.toReduce = append(c.toReduce, c.reducing[i])
				c.reducing = append(c.reducing[:i], c.reducing[i+1:]...)
				i--
			}
		}
		if c.mapDone && len(c.reducing) == 0 && len(c.toReduce) == 0 {
			c.mutex.Unlock()
			return
		}
		c.mutex.Unlock()
		time.Sleep(10 * time.Second)

	}

}
