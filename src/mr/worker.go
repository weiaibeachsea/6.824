package mr

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/rpc"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"time"

	. "6.824/common"
)

//
// Map functions return a slice of KeyValue.
//
type KeyValue struct {
	Key   string
	Value string
}

// for sorting by key.
type ByKey []KeyValue

// for sorting by key.
func (a ByKey) Len() int           { return len(a) }
func (a ByKey) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByKey) Less(i, j int) bool { return a[i].Key < a[j].Key }

//
// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
//
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32() & 0x7fffffff)
}

//
// main/mrworker.go calls this function.
//
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {

	// Your worker implementation here.
	for {
		taskInfo := AskMission()
		switch taskInfo.State {
		case MapState:
			{
				doMap(mapf, taskInfo)
				break
			}
		case ReduceState:
			{
				doReduce(reducef, taskInfo)
				break
			}
		case WaitState:
			{
				time.Sleep(10 * time.Second)
				break
			}
		case EndState:
			{
				log.Printf("Master all tasks complete. Nothing to do...")
				return
			}
		}

	}
	// uncomment to send the Example RPC to the coordinator.
	// CallExample()

}
func doMap(mapf func(string, string) []KeyValue, taskInfo *TaskInfo) {
	intermediate := []KeyValue{}
	file, err := os.Open(taskInfo.FileName)
	if err != nil {
		log.Fatalf("cannot open %+v", taskInfo)
	}
	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("cannot read %+v", taskInfo)
	}
	file.Close()
	kva := mapf(taskInfo.FileName, string(content))
	intermediate = append(intermediate, kva...)
	output := make([]*os.File, taskInfo.NReduce)
	enc := make([]*json.Encoder, taskInfo.NReduce)
	for i := 0; i < taskInfo.NReduce; i++ {
		file, err := ioutil.TempFile("./", "mr-tmp-*")
		if err != nil {
			log.Fatalf(err.Error())
		}
		output[i] = file
		enc[i] = json.NewEncoder(file)
	}

	for _, v := range intermediate {
		err = enc[ihash(v.Key)%taskInfo.NReduce].Encode(v)
		if err != nil {
			log.Printf("write failed, key=%s, value=%s", v.Key, v.Value)
			panic("Json encode failed")
		}
	}
	for i := 0; i < taskInfo.NReduce; i++ {
		file = output[i]
		name := file.Name()
		err = file.Close()
		if err != nil {
			log.Fatalf("cannot close")
		}
		outname := fmt.Sprintf("mr-%v-%v", taskInfo.FileIndex, i)
		err = os.Rename(filepath.Join(name), outname)
		if err != nil {
			log.Fatalf("cannot rename")
		}
	}
	DoneMission(taskInfo)
}
func doReduce(reducef func(string, []string) string, taskInfo *TaskInfo) {
	innameprefix := "mr-"
	innamesuffix := "-" + strconv.Itoa(taskInfo.PartIndex)
	var kva []KeyValue
	for i := 0; i < taskInfo.NFiles; i++ {
		file, err := os.Open(innameprefix + strconv.Itoa(i) + innamesuffix)
		if err != nil {
			log.Fatalf("cannot open %s\n", file.Name())
		}
		dec := json.NewDecoder(file)
		for {
			var kv KeyValue
			if err := dec.Decode(&kv); err != nil {
				break
			}
			kva = append(kva, kv)
		}
		file.Close()
	}
	sort.Sort(ByKey(kva))

	ofile, err := ioutil.TempFile("./", "mr-*")
	if err != nil {
		fmt.Printf("Create output file %v failed: %v\n", ofile, err)
		panic("Create file error")
	}
	//
	// call Reduce on each distinct key in intermediate[],
	// and print the result to mr-out-0.
	//
	i := 0
	for i < len(kva) {
		j := i + 1
		for j < len(kva) && kva[j].Key == kva[i].Key {
			j++
		}
		values := []string{}
		for k := i; k < j; k++ {
			values = append(values, kva[k].Value)
		}
		output := reducef(kva[i].Key, values)

		// this is the correct format for each line of Reduce output.
		fmt.Fprintf(ofile, "%v %v\n", kva[i].Key, output)

		i = j
	}
	outname := "mr-out-" + strconv.Itoa(taskInfo.PartIndex)
	os.Rename(filepath.Join(ofile.Name()), outname)
	ofile.Close()
	DoneMission(taskInfo)
}

//
// example function to show how to make an RPC call to the coordinator.
//
// the RPC argument and reply types are defined in rpc.go.
//
func CallExample() {

	// declare an argument structure.
	args := ExampleArgs{}

	// fill in the argument(s).
	args.X = 99

	// declare a reply structure.
	reply := ExampleReply{}

	// send the RPC request, wait for the reply.
	// the "Coordinator.Example" tells the
	// receiving server that we'd like to call
	// the Example() method of struct Coordinator.
	ok := call("Coordinator.Example", &args, &reply)
	if ok {
		// reply.Y should be 100.
		fmt.Printf("reply.Y %v\n", reply.Y)
	} else {
		fmt.Printf("call failed!\n")
	}
}

func AskMission() *TaskInfo {
	args := ExampleArgs{}
	reply := TaskInfo{}
	ok := call("Coordinator.AskMission", &args, &reply)
	if ok {
		// reply.Y should be 100.
		fmt.Printf("ask successfully\n")
		fmt.Printf("%+v\n", reply)
		return &reply
	} else {
		fmt.Printf("call failed!\n")
	}
	return nil
}

func DoneMission(taskInfo *TaskInfo) {
	args := taskInfo
	reply := TaskInfo{}
	ok := call("Coordinator.DoneMission", &args, &reply)
	if ok {
		// reply.Y should be 100.
		fmt.Printf("%+v\n", args)
		fmt.Printf("done successfully\n")
		return
	} else {
		fmt.Printf("call failed!\n")
	}
	// return
}

//
// send an RPC request to the coordinator, wait for the response.
// usually returns true.
// returns false if something goes wrong.
//
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := coordinatorSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
