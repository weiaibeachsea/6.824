package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	//	"bytes"
	"bytes"
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"

	//	"6.824/labgob"
	"6.824/labgob"
	"6.824/labrpc"
)

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
// in part 2D you'll want to send other kinds of messages (e.g.,
// snapshots) on the applyCh, but set CommandValid to false for these
// other uses.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int

	// For 2D:
	SnapshotValid bool
	Snapshot      []byte
	SnapshotTerm  int
	SnapshotIndex int
}

//
// A Go object implementing a single Raft peer.
//
type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]
	dead      int32               // set by Kill()

	// Your data here (2A, 2B, 2C).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.

	// Follower/Candidate/Leader
	state int
	c     *sync.Cond
	// Persistent state
	currentTerm int
	votedFor    int
	log         []Log

	snapshotLastIncludedIndex int
	snapshotlastIncludedTerm  int
	snapshot                  []byte

	// Volatile state on all servers:
	commitIndex int
	lastApplied int
	// Volatile state on leaders:
	nextIndex  []int
	matchIndex []int

	ReceiveAppendEntries chan int
	GrantingVote         chan int
	VotePass             chan int
	applyCh              chan ApplyMsg
}

const (
	Follower  = 0
	Candidate = 1
	Leader    = 2
)

type Log struct {
	Command interface{}
	Term    int
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (2A).
	rf.mu.Lock()
	term = rf.currentTerm
	isleader = (rf.state == Leader)
	log.Printf("getstate(%d)是%d\n", rf.me, rf.state)
	rf.mu.Unlock()
	return term, isleader
}

//
// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
//
func (rf *Raft) persist() {
	// Your code here (2C).
	// Example:
	// w := new(bytes.Buffer)
	// e := labgob.NewEncoder(w)
	// e.Encode(rf.xxx)
	// e.Encode(rf.yyy)
	// data := w.Bytes()
	// rf.persister.SaveRaftState(data)
	rf.mu.Lock()
	defer rf.mu.Unlock()
	log.Printf("%d持久化", rf.me)
	w := new(bytes.Buffer)
	e := labgob.NewEncoder(w)
	e.Encode(rf.currentTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.log)
	e.Encode(rf.snapshotLastIncludedIndex)
	log.Printf("%d 的 snapshotLastIncludedIndex = %d", rf.me, rf.snapshotLastIncludedIndex)
	e.Encode(rf.snapshotlastIncludedTerm)
	// e.Encode(rf.snapshotLastIncludedIndex)
	// e.Encode(rf.snapshot)
	data := w.Bytes()
	rf.persister.SaveRaftState(data)
}

//
// restore previously persisted state.
//
func (rf *Raft) readPersist(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}
	// Your code here (2C).
	// Example:
	// r := bytes.NewBuffer(data)
	// d := labgob.NewDecoder(r)
	// var xxx
	// var yyy
	// if d.Decode(&xxx) != nil ||
	//    d.Decode(&yyy) != nil {
	//   error...
	// } else {
	//   rf.xxx = xxx
	//   rf.yyy = yyy
	// }
	rf.mu.Lock()
	defer rf.mu.Unlock()
	r := bytes.NewBuffer(data)
	d := labgob.NewDecoder(r)
	var currentTerm int
	var votedFor int
	var logArr []Log
	// var offset int
	// var snapshot []byte
	var snapshotLastIncludedIndex int
	var snapshotlastIncludedTerm int
	// d.Decode(&offset) != nil ||
	// d.Decode(&snapshot) != nil ||
	// d.Decode(&snapshotLastIncludedIndex) != nil ||
	// d.Decode(&snapshotlastIncludedTerm) != nil
	// var snapshot []byte
	if d.Decode(&currentTerm) != nil ||
		d.Decode(&votedFor) != nil ||
		d.Decode(&logArr) != nil ||
		d.Decode(&snapshotLastIncludedIndex) != nil ||
		d.Decode(&snapshotlastIncludedTerm) != nil {
		//   error...
		log.Panicf("Decode失败")
	} else {
		rf.currentTerm = currentTerm
		rf.votedFor = votedFor
		rf.log = logArr
		// rf.snapshot = snapshot
		rf.snapshotLastIncludedIndex = snapshotLastIncludedIndex
		rf.snapshotlastIncludedTerm = snapshotlastIncludedTerm
		// rf.snapshot = snapshot
	}
	log.Printf("%d 的 snapshotLastIncludedIndex = %d", rf.me, rf.snapshotLastIncludedIndex)

}

func (rf *Raft) persistStateAndSnapshot() {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	log.Printf("%d持久化snapshot", rf.me)

	w := new(bytes.Buffer)
	e := labgob.NewEncoder(w)
	e.Encode(rf.currentTerm)
	e.Encode(rf.votedFor)
	e.Encode(rf.log)
	e.Encode(rf.snapshotLastIncludedIndex)
	e.Encode(rf.snapshotlastIncludedTerm)

	// e.Encode(rf.snapshotLastIncludedIndex)

	// Buffer := new(bytes.Buffer)
	// LabEncoder := labgob.NewEncoder(Buffer)
	// LabEncoder.Encode(rf.snapshotLastIncludedIndex)
	// LabEncoder.Encode(rf.snapshot)
	// LabEncoder.Encode(rf.snapshotlastIncludedTerm)

	data := w.Bytes()
	// snapshot := Buffer.Bytes()

	rf.persister.SaveStateAndSnapshot(data, rf.snapshot)
	log.Printf("%d 持久化StateAndSnapshot成功, snapshotLastIncludedIndex = %d", rf.me, rf.snapshotLastIncludedIndex)
}

func (rf *Raft) readPersistSnapshot(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}
	rf.mu.Lock()
	defer rf.mu.Unlock()
	// r := bytes.NewBuffer(data)
	// d := labgob.NewDecoder(r)
	// var snapshot []byte
	// var snapshotLastIncludedIndex int
	// var snapshotlastIncludedTerm int
	// // var logArr []Log

	// if d.Decode(&snapshotLastIncludedIndex) != nil ||

	// 	d.Decode(&snapshot) != nil ||
	// 	d.Decode(&snapshotlastIncludedTerm) != nil {
	// 	//   error...
	// 	log.Panicf("Decode失败")
	// } else {
	// 	rf.snapshot = snapshot
	// 	rf.snapshotLastIncludedIndex = snapshotLastIncludedIndex
	// 	rf.snapshotlastIncludedTerm = snapshotlastIncludedTerm
	// 	// rf.log = logArr
	// }
	rf.snapshot = data

}

//
// A service wants to switch to snapshot.  Only do so if Raft hasn't
// have more recent info since it communicate the snapshot on applyCh.
//
func (rf *Raft) CondInstallSnapshot(lastIncludedTerm int, lastIncludedIndex int, snapshot []byte) bool {

	// Your code here (2D).

	return true
}

// the service says it has created a snapshot that has
// all info up to and including index. this means the
// service no longer needs the log through (and including)
// that index. Raft should now trim its log as much as possible.
func (rf *Raft) Snapshot(index int, snapshot []byte) {
	// Your code here (2D).
	go func(index int, snapshot []byte) {
		log.Printf("%d正在调用snapshot(), index = %d", rf.me, index)
		rf.mu.Lock()
		log.Printf("%d调用snapshot拿到了lock", rf.me)

		defer rf.mu.Unlock()
		// rf.applyCh <- ApplyMsg{
		// 	CommandValid: false,
		// 	// Command:       nil,
		// 	// CommandIndex:  index,
		// 	SnapshotValid: true,
		// 	Snapshot:      snapshot,
		// 	SnapshotTerm:  rf.currentTerm,
		// 	SnapshotIndex: index,
		// }
		if rf.covertToArrIndex(index) < 0 {
			return
		}
		rf.snapshotlastIncludedTerm = rf.log[rf.covertToArrIndex(index)].Term
		rf.log[0].Term = rf.log[index-rf.snapshotLastIncludedIndex].Term
		rf.log = rf.log[index-rf.snapshotLastIncludedIndex:]
		rf.snapshotLastIncludedIndex = index
		rf.snapshot = snapshot
		go rf.persistStateAndSnapshot()
		log.Printf("%d调用snapshot()完成", rf.me)
	}(index, snapshot)

}

type InstallSnapshotArgs struct {
	Term              int
	LeaderId          int
	LastIncludedIndex int //the snapshot replaces all entries up through and including this index
	LastIncludedTerm  int //term of lastIncludedIndex
	// offset byte offset where chunk is positioned in the snapshot file
	Data []byte //raw bytes of the snapshot chunk, starting atoffset
	// done
}

type InstallSnapshotReply struct {
	Success bool
	Term    int
}

func (rf *Raft) InstallSnapshot(args *InstallSnapshotArgs, reply *InstallSnapshotReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()

	reply.Term = rf.currentTerm

	//  Reply immediately if term < currentTerm
	if args.Term < rf.currentTerm {
		reply.Success = false
		return
	}
	if args.Term > rf.currentTerm {
		rf.currentTerm = args.Term
		rf.state = Follower
		log.Printf("%d遇到高term,重置为Follower", rf.me)
		rf.votedFor = -1
		go rf.persistStateAndSnapshot()
	}
	// XXX
	// if rf.index >= args.LastIncludedIndex {
	// 	// reply.Term = rf.currentTerm
	// 	return
	// }

	localLogIndex := rf.covertToArrIndex(args.LastIncludedIndex)
	// rf.index = args.LastIncludedIndex
	log.Printf("%d: localLogIndex = %d, len(rf.log)= %d", rf.me, localLogIndex, len(rf.log))
	if localLogIndex > len(rf.log)-1 {
		rf.log = []Log{{
			Command: nil,
			Term:    args.LastIncludedTerm,
		}}
	} else if localLogIndex < 0 { //应该没有可能<0
		// rf.log = nil
		log.Printf("%d收到来自%d的旧快照,快照 LastIncludedIndex =%d, LastIncludedTerm = %d", rf.me, args.LeaderId, args.LastIncludedIndex, args.LastIncludedTerm)
		reply.Success = false
		return //XXX
	} else if rf.log[localLogIndex].Term == args.LastIncludedTerm {
		// If existing log entry has same index and term as snapshot’s last included entry, retain log entries following it and reply
		rf.log = rf.log[localLogIndex:]
		// XXX reply??
		// return
	} else {
		rf.log = []Log{{
			Command: nil,
			Term:    args.LastIncludedTerm,
		}}
	}
	log.Printf("%d: log[0].Term = %d", rf.me, rf.log[0].Term)
	rf.snapshotLastIncludedIndex = args.LastIncludedIndex
	rf.snapshot = args.Data
	rf.snapshotlastIncludedTerm = args.LastIncludedTerm
	//  Save snapshot file, discard any existing or partial snapshot with a smaller index
	go rf.persistStateAndSnapshot()

	reply.Success = true
	log.Printf("%d接受了%d的快照,term=%d, LastIncludedIndex =%d, LastIncludedTerm = %d", rf.me, args.LeaderId, args.Term, args.LastIncludedIndex, args.LastIncludedTerm)
	if args.LastIncludedIndex <= rf.lastApplied {
		log.Printf("%d 跳过 %d的快照,term=%d, LastIncludedIndex =%d, LastIncludedTerm = %d , lastApplied = %d ", rf.me, args.LeaderId, args.Term, args.LastIncludedIndex, args.LastIncludedTerm, rf.lastApplied)
	} else {
		rf.applyCh <- ApplyMsg{
			CommandValid:  false,
			Command:       nil,
			CommandIndex:  0,
			SnapshotValid: true,
			Snapshot:      args.Data,
			SnapshotTerm:  args.LastIncludedTerm,
			SnapshotIndex: args.LastIncludedIndex,
		}
		rf.lastApplied = args.LastIncludedIndex
		rf.commitIndex = Max(args.LastIncludedIndex, rf.commitIndex)
		log.Printf("%d应用了%d的快照,term=%d, LastIncludedIndex =%d, LastIncludedTerm = %d", rf.me, args.LeaderId, args.Term, args.LastIncludedIndex, args.LastIncludedTerm)
	}

}

func (rf *Raft) sendInstallSnapshot(server int, args *InstallSnapshotArgs, reply *InstallSnapshotReply) bool {
	ok := rf.peers[server].Call("Raft.InstallSnapshot", args, reply)
	return ok
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term         int
	CandidateId  int
	LastLogIndex int
	LastLogTerm  int
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (2A).
	Term        int
	VoteGranted bool
}

//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).
	//TODO:更新些什么，重置计时器？
	log.Printf("%d收到%d的投票请求,argsTerm=%d\n", rf.me, args.CandidateId, args.Term)

	rf.mu.Lock()
	defer rf.mu.Unlock()
	// log.Printf("%d收到%d的投票请求,argsTerm=%d,currentTerm=%d\n", rf.me, args.CandidateId, args.Term, rf.currentTerm)

	if args.Term < rf.currentTerm {
		reply.VoteGranted = false
		reply.Term = rf.currentTerm
		log.Printf("%d拒绝给%d投票\n", rf.me, args.CandidateId)
		return
	}
	if args.Term > rf.currentTerm {
		rf.currentTerm = args.Term
		rf.state = Follower
		log.Printf("%d遇到高term,重置为Follower", rf.me)
		rf.votedFor = -1
		go rf.persistStateAndSnapshot()
	}
	lastLogTerm := rf.snapshotlastIncludedTerm
	if len(rf.log) != 0 {
		lastLogTerm = rf.log[len(rf.log)-1].Term
	}
	log.Printf("%d收到%d的投票请求,args=%v,currentTerm=%d\n", rf.me, args.CandidateId, args, rf.currentTerm)

	if (rf.votedFor == -1 || rf.votedFor == args.CandidateId) &&
		(args.LastLogTerm > lastLogTerm ||
			((args.LastLogTerm == lastLogTerm) && (args.LastLogIndex >= rf.covertToVirtualIndex(len(rf.log)-1)))) {
		reply.VoteGranted = true
		reply.Term = rf.currentTerm
		rf.GrantingVote <- 1
		rf.votedFor = args.CandidateId
		go rf.persistStateAndSnapshot()

		// rf.ticker()
		log.Printf("%d把票投给%d\n", rf.me, args.CandidateId)
		return
	}
	reply.VoteGranted = false
	reply.Term = rf.currentTerm
	log.Printf("%d拒绝给%d投票\n", rf.me, args.CandidateId)
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}

//
// example AppendEntries RPC arguments structure.
// field names must start with capital letters!
//
type AppendEntriesArgs struct {
	Term         int
	LeaderId     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []Log
	LeaderCommit int
}

//
// example AppendEntries RPC reply structure.
// field names must start with capital letters!
//
type AppendEntriesReply struct {
	Term    int
	Success bool
	XTerm   int
	XIndex  int
	XLen    int
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	// TODO:重置选举超时
	rf.mu.Lock()
	defer rf.mu.Unlock()
	log.Printf("%d 收到来自 %d 的心跳: %v", rf.me, args.LeaderId, args)
	if args.Term < rf.currentTerm {
		reply.Success = false
		reply.Term = rf.currentTerm
		return
	}
	// XXX是不是放这
	rf.ReceiveAppendEntries <- 1
	if args.Term > rf.currentTerm {
		rf.currentTerm = args.Term
		rf.state = Follower
		log.Printf("%d遇到高term,重置为Follower", rf.me)
		rf.votedFor = -1
		go rf.persistStateAndSnapshot()
	}
	// XXX这么写对么:
	// 应该需要判断为Candidate，不然leader收到自己的心跳重置为follower是不对的
	//
	// Candidates：If AppendEntries RPC received from new leader: convert to follower
	if rf.state == Candidate && args.Term == rf.currentTerm {
		rf.state = Follower
		log.Printf("%d作为candidate遇到相等term的心跳(新leader),重置为Follower", rf.me)
		// XXX VoteFor好像维持原状
		// rf.votedFor = -1
	}
	if rf.covertToVirtualIndex(len(rf.log)-1) < args.PrevLogIndex {
		*reply = AppendEntriesReply{
			Term:    rf.currentTerm,
			Success: false,
			XTerm:   -1,
			XIndex:  -1,
			XLen:    args.PrevLogIndex - rf.covertToVirtualIndex(len(rf.log)) + 1,
		}
		log.Printf("-----%d收到%d的心跳,args.PrevLogIndex=%d,太长,空白长度为%d", rf.me, args.LeaderId, args.PrevLogIndex, reply.XLen)
		return
	}
	if rf.covertToArrIndex(args.PrevLogIndex) < 0 {

	} else if rf.log[rf.covertToArrIndex(args.PrevLogIndex)].Term != args.PrevLogTerm {
		i := rf.covertToArrIndex(args.PrevLogIndex)
		for ; i >= 0; i-- {
			if rf.log[i].Term != rf.log[rf.covertToArrIndex(args.PrevLogIndex)].Term {
				break
			}
		}
		*reply = AppendEntriesReply{
			Term:    rf.currentTerm,
			Success: false,
			XTerm:   rf.log[rf.covertToArrIndex(args.PrevLogIndex)].Term,
			XIndex:  rf.covertToVirtualIndex(i + 1),
			XLen:    0,
		}
		// for i, v := range rf.log {
		// 	log.Printf("%d的%d位置日志term为%d", rf.me, i, v)
		// }
		return
	}
	// }
	i := Max(0, rf.snapshotLastIncludedIndex-args.PrevLogIndex)
	for ; i < len(rf.log)-1-rf.covertToArrIndex(args.PrevLogIndex) && i < len(args.Entries); i++ {
		// for ; i < len(rf.log)-args.PrevLogIndex-1 && i < len(args.Entries); i++ {
		if rf.log[rf.covertToArrIndex(i+args.PrevLogIndex+1)].Term != args.Entries[i].Term {
			// if rf.log[i+args.PrevLogIndex+1].Term != args.Entries[i].Term {
			rf.log = rf.log[:rf.covertToArrIndex(i+args.PrevLogIndex+1)]
			break
		}
	}
	for ; i < len(args.Entries); i++ {
		rf.log = append(rf.log, args.Entries[i])
	}
	if args.LeaderCommit > rf.commitIndex {
		rf.commitIndex = Min(args.LeaderCommit, args.PrevLogIndex+len(args.Entries))
		rf.c.Broadcast()
	}
	reply.Success = true
	reply.Term = rf.currentTerm
	log.Printf("%d接受了%d的消息", rf.me, args.LeaderId)
	// for i := 0; i < len(rf.log); i++ {
	// 	log.Printf("%d的%d位置日志为:%d.%d", rf.me, rf.covertToVirtualIndex(i), rf.log[i].Term, rf.log[i].Command)
	// }
	go rf.persistStateAndSnapshot()
}

func (rf *Raft) covertToArrIndex(VirtualIndex int) int {
	// ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	return VirtualIndex - rf.snapshotLastIncludedIndex
}
func (rf *Raft) covertToVirtualIndex(ArrIndex int) int {
	return ArrIndex + rf.snapshotLastIncludedIndex
}

func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	return ok
}

func Min(a int, b int) int {
	if a > b {
		return b
	} else {
		return a
	}
}
func Max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	term := -1
	isLeader := false

	// Your code here (2B).
	if rf.killed() {
		return index, term, isLeader
	}
	rf.mu.Lock()
	defer rf.mu.Unlock()
	isLeader = (rf.state == Leader)
	if !isLeader {
		return index, term, isLeader
	}
	index = rf.covertToVirtualIndex(len(rf.log)) //XXX
	term = rf.currentTerm

	log.Printf("Raft %d 接到命令为%v", rf.me, command)
	// log.Println(command)

	rf.log = append(rf.log, Log{
		Command: command,
		Term:    term,
	})
	go rf.sendAppendEntriesToFollowers()
	go rf.persistStateAndSnapshot()
	// start the agreement and return immediately.
	// go func(index int) {
	// 	// send AppendEntries RPC
	// 	for i := 0; i < len(rf.peers); i++ {
	// 		go func(i int) {
	// 			// If AppendEntries fails because of log inconsistency: decrement nextIndex and retry
	// 			for !rf.killed() {
	// 				rf.mu.Lock()
	// 				if rf.state != Leader {
	// 					rf.mu.Unlock()
	// 					return
	// 				}
	// 				log.Printf("%v", rf.nextIndex)
	// 				args := &AppendEntriesArgs{
	// 					Term:         rf.currentTerm,
	// 					LeaderId:     rf.me,
	// 					PrevLogIndex: rf.nextIndex[i] - 1,            //XXX
	// 					PrevLogTerm:  rf.log[rf.nextIndex[i]-1].Term, //XXX
	// 					Entries:      rf.log[rf.nextIndex[i]:],       //XXX
	// 					LeaderCommit: rf.commitIndex,
	// 				}
	// 				// If last log index ≥ nextIndex for a follower: send AppendEntries RPC with log entries starting at nextIndex
	// 				if len(rf.log)-1 < rf.nextIndex[i] {
	// 					rf.mu.Unlock()
	// 					return
	// 				}
	// 				rf.mu.Unlock()

	// 				reply := &AppendEntriesReply{}
	// 				rf.sendAppendEntries(i, args, reply)
	// 				// TODO:检查reply
	// 				log.Printf("%d对%d发送的AppendEntries反应为:%v", i, rf.me, reply.Success)
	// 				rf.mu.Lock()
	// 				defer rf.mu.Unlock()
	// 				if reply.Term > rf.currentTerm {
	// 					rf.currentTerm = reply.Term
	// 					log.Printf("%d降级", rf.me)
	// 					return
	// 				}
	// 				// If successful: update nextIndex and matchIndex for follower
	// 				if reply.Success {
	// 					rf.nextIndex[i] = args.PrevLogIndex + len(args.Entries) + 1 //XXX
	// 					rf.matchIndex[i] = args.PrevLogIndex + len(args.Entries)    //XXX
	// 					log.Printf("%d为leader:rf.matchIndex[%d]:%v", rf.me, i, rf.matchIndex[i])
	// 					return
	// 				} else {
	// 					// If AppendEntries fails because of log inconsistency: decrement nextIndex and retry
	// 					rf.nextIndex[i] = args.PrevLogIndex - 1 //XXX
	// 				}
	// 			}
	// 		}(i)
	// 	}

	// 	// for !rf.killed() {
	// 	// 	// If there exists an N such that N > commitIndex, a majority of matchIndex[i] ≥ N, and log[N].term == currentTerm:
	// 	// 	// set commitIndex = N
	// 	// 	rf.mu.Lock()

	// 	// 	if rf.state != Leader {
	// 	// 		rf.mu.Unlock()
	// 	// 		return
	// 	// 	}
	// 	// 	log.Printf("%d 的commitIndex正在计算,当前为%d", rf.me, rf.commitIndex)
	// 	// 	N := rf.commitIndex + 1
	// 	// 	for ; N < len(rf.log); N++ {
	// 	// 		cnt := 0
	// 	// 		for j := 0; j < len(rf.peers); j++ {
	// 	// 			if rf.matchIndex[j] >= N {
	// 	// 				cnt++
	// 	// 			}
	// 	// 		}
	// 	// 		if cnt > len(rf.peers)/2+1 && rf.log[N].Term == rf.currentTerm {
	// 	// 			rf.commitIndex = N
	// 	// 			log.Printf("%d 的commitIndex=%d", rf.me, N)
	// 	// 			// if N >= index {
	// 	// 			// 	rf.applyCh <- ApplyMsg{
	// 	// 			// 		CommandValid: true,
	// 	// 			// 		Command:      command,
	// 	// 			// 		CommandIndex: index,
	// 	// 			// 	}
	// 	// 			// 	rf.mu.Unlock()
	// 	// 			// 	return
	// 	// 			// }
	// 	// 		}
	// 	// 	}
	// 	// 	rf.mu.Unlock()

	// 	// 	time.Sleep(10 * time.Millisecond)
	// 	// }
	// }(index)

	return index, term, isLeader
}

//
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
	// rf.mu.Lock()
	// defer rf.mu.Unlock()
	log.Printf("%d死了", rf.me)
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

// The ticker go routine starts a new election if this peer hasn't received
// heartsbeats recently.
func (rf *Raft) ticker() {
	for rf.killed() == false {

		// Your code here to check if a leader election should
		// be started and to randomize sleeping time using
		// time.Sleep().
		// TODO:
		select {
		case <-rf.ReceiveAppendEntries:
			{
				// go func() {
				// 	rf.mu.Lock()
				// 	// log.Printf("%d加锁", rf.me)

				// 	rf.state = Follower
				// 	log.Printf("%d重置", rf.me)
				// 	rf.mu.Unlock()
				// 	// log.Printf("%d没解锁码?\n", rf.me)

				// }()
			}

		case <-rf.GrantingVote:
			// XXX这么写对吗
		case <-rf.VotePass:
			{
				go func() {
					rf.mu.Lock()
					defer rf.mu.Unlock()
					// log.Printf("%d加锁", rf.me)
					if rf.state != Candidate {
						return
					}
					rf.state = Leader
					log.Printf("%d成为leader\n", rf.me)
					for i := 0; i < len(rf.peers); i++ {
						// (initialized to leader last log index + 1)
						rf.nextIndex[i] = rf.covertToVirtualIndex(len(rf.log))
						log.Printf("rf[%d].nextIndex[%d]=%d", rf.me, i, rf.nextIndex[i])
						rf.matchIndex[i] = rf.covertToVirtualIndex(0)
					}
					go rf.sendAppendEntriesWithTicker()
					// rf.mu.Unlock()
					// log.Printf("%d没解锁码?\n", rf.me)
					go rf.updateCommitIndex()
				}()
			}

		case <-time.After(time.Duration(rand.Int31n(150)+300) * time.Millisecond):
			{
				go rf.startElection()
				// go func() {
				// 	rf.mu.Lock()
				// 	// log.Printf("%d加锁", rf.me)
				// 	state := rf.state
				// 	log.Printf("%d状态为%d", rf.me, rf.state)
				// 	rf.mu.Unlock()
				// 	// log.Printf("%d没解锁码?\n", rf.me)

				// 	if state != Leader {
				// 		rf.mu.Lock()
				// 		// log.Printf("%d加锁", rf.me)

				// 		log.Printf("%d正在竞选,term=%d\n", rf.me, rf.currentTerm)
				// 		rf.state = Candidate
				// 		// Increment currentTerm
				// 		rf.currentTerm++
				// 		// Vote for self
				// 		getVote := 1
				// 		rf.votedFor = rf.me
				// 		// TODO:Reset election timer
				// 		// rf.ReceiveAppendEntries = false
				// 		countVote := make(chan int, len(rf.peers))

				// 		// Send RequestVote RPCs to all other servers
				// 		for i := 0; i < len(rf.peers); i++ {
				// 			go func(i int, countVote chan int) {
				// 				// BUG:怎么排除自己?
				// 				if i == rf.me {
				// 					return
				// 				}
				// 				rf.mu.Lock()
				// 				// log.Printf("%d加锁", rf.me)

				// 				args := &RequestVoteArgs{
				// 					Term:         rf.currentTerm,
				// 					CandidateId:  rf.me,
				// 					LastLogIndex: len(rf.log) - 1,
				// 					LastLogTerm:  rf.log[len(rf.log)-1].Term,
				// 				}
				// 				log.Printf("%d请求%d投票,argsTerm=%d\n", rf.me, i, args.Term)
				// 				rf.mu.Unlock()
				// 				// log.Printf("%d没解锁码?\n", rf.me)

				// 				reply := &RequestVoteReply{}
				// 				rf.sendRequestVote(i, args, reply)
				// 				log.Printf("%d收到%d投票结果:%v,args.term=%d,reply.term=%d\n", rf.me, i, reply.VoteGranted, args.Term, reply.Term)
				// 				if reply.VoteGranted {
				// 					rf.mu.Lock()
				// 					// log.Printf("%d加锁", rf.me)

				// 					getVote++
				// 					if getVote >= len(rf.peers)/2+1 && rf.currentTerm == args.Term && rf.state != Leader && len(rf.VotePass) == 0 {
				// 						log.Printf("%d得到了大多数票\n", rf.me)
				// 						rf.VotePass <- 1
				// 						log.Printf("%d得到了大多数票...\n", rf.me)
				// 					}
				// 					rf.mu.Unlock()
				// 					// log.Printf("%d没解锁码?\n", rf.me)

				// 				}
				// 			}(i, countVote)
				// 		}
				// 		rf.mu.Unlock()
				// 		// log.Printf("%d没解锁码?\n", rf.me)

				// 	}
				// }()

				// select {
				// case <-countVote:
				// 	{
				// 		go func() {
				// 			rf.mu.Lock()
				// 			// log.Printf("%d加锁", rf.me)

				// 			rf.state = Leader
				// 			log.Printf("%d成为leader\n", rf.me)
				// 			for i := 0; i < len(rf.peers); i++ {
				// 				// (initialized to leader last log index + 1)
				// 				rf.nextIndex[i] = len(rf.log)
				// 				rf.matchIndex[i] = 0
				// 			}
				// 			go rf.Heartbeat()
				// 			rf.mu.Unlock()
				// 			// log.Printf("%d没解锁码?\n", rf.me)

				// 		}()
				// 	}
				// case <-rf.ReceiveAppendEntries:
				// 	{
				// 		go func() {
				// 			rf.mu.Lock()
				// 			// log.Printf("%d加锁", rf.me)

				// 			rf.state = Follower
				// log.Printf("%d降级",rf.me)
				// 			rf.mu.Unlock()
				// 			// log.Printf("%d没解锁码?\n", rf.me)

				// 		}()

				// 	}
				// case <-time.After(time.Duration(rand.Int31n(150)+300) * time.Millisecond):
				// 	// rf.mu.Lock()
				// 	log.Printf("%d加锁", rf.me)

				// }
			}
			// rf.mu.Unlock()
			// log.Printf("%d没解锁码?\n", rf.me)

		}
	}
	// rf.mu.Lock()
	// if rf.state != Leader && (rf.votedFor == -1 || !rf.ReceiveAppendEntries) {
	// 	log.Printf("%d正在竞选,term=%d\n", rf.me, rf.currentTerm)

	// 	time.Sleep(time.Duration(rand.Int31n(150)+300) * time.Millisecond)
	// 	rf.mu.Lock()
	// 	log.Printf("%d得到票数:%d\n", rf.me, getVote)
	// 	if getVote >= len(rf.peers)/2+1 && rf.state == Candidate {
	// 		rf.state = Leader
	// 		go rf.Heartbeat()
	// 	} else if rf.ReceiveAppendEntries { // ?
	// 		rf.state = Follower
	// log.Printf("%d降级", rf.me)
	// 	} else {
	// 		rf.mu.Unlock()
	// 		continue
	// 	}

	// }
	// rf.ReceiveAppendEntries = false
	// rf.mu.Unlock()

	// time.Sleep(time.Duration(rand.Int31n(150)+300) * time.Millisecond)

}
func (rf *Raft) startElection() {
	// for !rf.killed() {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	// log.Printf("%d加锁", rf.me)
	state := rf.state
	log.Printf("%d状态为%d", rf.me, rf.state)
	// log.Printf("%d没解锁码?\n", rf.me)

	if state != Leader {
		// rf.mu.Lock()
		// log.Printf("%d加锁", rf.me)

		// convert to candidate
		rf.state = Candidate
		// Increment currentTerm
		rf.currentTerm++
		log.Printf("%d正在竞选,newTerm=%d\n", rf.me, rf.currentTerm)

		// Vote for self
		// getVote := 0
		rf.votedFor = rf.me
		go rf.persistStateAndSnapshot()

		// TODO:Reset election timer
		// rf.ReceiveAppendEntries = false
		countVote := make(chan int, len(rf.peers))
		votePass := make(chan int)
		term := rf.currentTerm
		candidateId := rf.me
		lastLogIndex := rf.covertToVirtualIndex(len(rf.log) - 1)
		lastLogTerm := rf.snapshotlastIncludedTerm
		if len(rf.log) != 0 {
			lastLogTerm = rf.log[len(rf.log)-1].Term
		}
		// Send RequestVote RPCs to all other servers
		for i := 0; i < len(rf.peers); i++ {
			go func(i int, countVote chan int, term int, candidateId int, lastLogIndex int, lastLogTerm int) {
				// BUG:怎么排除自己?，不排除自己的话getVote应为0
				// if i == rf.me {
				// 	return
				// }
				// rf.mu.Lock()
				// log.Printf("%d加锁", rf.me)

				args := &RequestVoteArgs{
					Term:         term,
					CandidateId:  candidateId,
					LastLogIndex: lastLogIndex,
					LastLogTerm:  lastLogTerm,
				}
				log.Printf("%d请求%d投票,argsTerm=%d\n", candidateId, i, term)
				// rf.mu.Unlock()
				// log.Printf("%d没解锁码?\n", rf.me)

				reply := &RequestVoteReply{}
				ok := rf.sendRequestVote(i, args, reply)
				if !ok {
					log.Printf("%d没有收到%d的投票回应", rf.me, i)
					return
				}
				log.Printf("%d收到%d投票结果:%v,args.term=%d,reply.term=%d\n", candidateId, i, reply.VoteGranted, args.Term, reply.Term)
				rf.mu.Lock()
				defer rf.mu.Unlock()
				if reply.Term > rf.currentTerm {
					rf.currentTerm = reply.Term
					rf.state = Follower
					log.Printf("%d降级", rf.me)
					go rf.persistStateAndSnapshot()
					return
				}
				if reply.VoteGranted {
					// log.Printf("%d加锁", rf.me)
					// check that rf.currentTerm hasn't changed since the decision to become a candidate
					if term != rf.currentTerm {
						log.Printf("%d收到过期赞成票from%d,args.term=%d,cur.term=%d\n", candidateId, i, args.Term, rf.currentTerm)
						return
					}
					countVote <- i
					// getVote++
					// if getVote >= len(rf.peers)/2+1 && rf.state != Leader && len(rf.VotePass) == 0 {
					// 	log.Printf("%d得到了大多数票\n", rf.me)
					// 	rf.VotePass <- 1
					// 	log.Printf("%d得到了大多数票...\n", rf.me)
					// }
					// rf.mu.Unlock()
					// log.Printf("%d没解锁码?\n", rf.me)

				}
			}(i, countVote, term, candidateId, lastLogIndex, lastLogTerm)
		}
		go func(countVote chan int, peersNum int, votePass chan int) {
			for i := 0; i < peersNum/2+1; i++ {
				<-countVote
			}
			rf.VotePass <- 1
		}(countVote, len(rf.peers), votePass)
		// rf.mu.Unlock()
		// select {
		// case <-votePass:
		// 	go func() {
		// 		rf.mu.Lock()
		// 		// log.Printf("%d加锁", rf.me)
		// 		if rf.state != Candidate {
		// 			rf.mu.Unlock()
		// 			return
		// 		}
		// 		rf.state = Leader
		// 		log.Printf("%d成为leader\n", rf.me)
		// 		for i := 0; i < len(rf.peers); i++ {
		// 			// (initialized to leader last log index + 1)
		// 			rf.nextIndex[i] = len(rf.log)
		// 			rf.matchIndex[i] = 0
		// 		}
		// 		go rf.sendAppendEntriesWithTicker()
		// 		rf.mu.Unlock()
		// 		// log.Printf("%d没解锁码?\n", rf.me)
		// 		go rf.updateCommitIndex()
		// 	}()
		// 	return
		// case <-rf.ReceiveAppendEntries:
		// 	{
		// 		// go func() {
		// 		// 	rf.mu.Lock()
		// 		// 	rf.state = Follower
		// 		// 	log.Printf("%d作为candidate遇到相等term的心跳(新leader),重置为Follower", rf.me)
		// 		// 	rf.mu.Unlock()
		// 		// }()
		// 		return
		// 	}
		// case <-time.After(time.Duration(rand.Int31n(150)+300) * time.Millisecond):
		// 	continue
		// }
		// rf.mu.Unlock()
		// log.Printf("%d没解锁码?\n", rf.me)

	}

	// }

}

// }
func (rf *Raft) applyChWithTicker() {
	for !rf.killed() {
		rf.mu.Lock()
		for rf.commitIndex > rf.lastApplied {
			rf.lastApplied++
			rf.applyCh <- ApplyMsg{
				CommandValid: true,
				Command:      rf.log[rf.covertToArrIndex(rf.lastApplied)].Command,
				CommandIndex: rf.lastApplied,
			}
			log.Printf("%d应用了%d", rf.me, rf.lastApplied)
		}
		rf.mu.Unlock()
		time.Sleep(10 * time.Millisecond)
	}
}

func (rf *Raft) applyChWithCondition() {

	for !rf.killed() {

		rf.c.L.Lock()

		for rf.commitIndex <= rf.lastApplied {
			rf.c.Wait()
		}
		//    ... make use of condition ...
		applyMsgs := []ApplyMsg{}
		for rf.commitIndex > rf.lastApplied {
			rf.lastApplied++
			applyMsgs = append(applyMsgs, ApplyMsg{
				CommandValid: true,
				Command:      rf.log[rf.covertToArrIndex(rf.lastApplied)].Command,
				CommandIndex: rf.lastApplied,
			})
		}
		// 不能在持有锁的时候, 向通道发消息
		for i := 0; i < len(applyMsgs); i++ {
			log.Printf("%d正在应用%d", rf.me, applyMsgs[i].CommandIndex)
			rf.applyCh <- applyMsgs[i]
			log.Printf("%d应用了%d", rf.me, applyMsgs[i].CommandIndex)
		}
		rf.c.L.Unlock()
		log.Printf("%d没解锁码?\n", rf.me)

		// rf.mu.Unlock()

		// rf.mu.Lock()
		// for rf.commitIndex > rf.lastApplied {
		// 	rf.lastApplied++
		// 	rf.applyCh <- ApplyMsg{
		// 		CommandValid: true,
		// 		Command:      rf.log[rf.covertToArrIndex(rf.lastApplied)].Command,
		// 		CommandIndex: rf.lastApplied,
		// 	}
		// 	log.Printf("%d应用了%d", rf.me, rf.lastApplied)
		// }
		// rf.mu.Unlock()
		// time.Sleep(10 * time.Millisecond)
	}
	// rf.c.L.Unlock()

}

func (rf *Raft) updateCommitIndex() {
	for !rf.killed() {
		// If there exists an N such that N > commitIndex, a majority of matchIndex[i] ≥ N, and log[N].term == currentTerm:
		// set commitIndex = N
		rf.mu.Lock()

		if rf.state != Leader {
			rf.mu.Unlock()
			return
		}
		oldcommitIndex := rf.commitIndex
		log.Printf("%d 的commitIndex正在计算,当前为%d", rf.me, rf.commitIndex)
		N := rf.commitIndex + 1 //2
		// <3
		for ; N < rf.covertToVirtualIndex(len(rf.log)); N++ {
			cnt := 0
			for j := 0; j < len(rf.peers); j++ {
				if rf.matchIndex[j] >= N {
					cnt++
				}
			}
			if cnt >= len(rf.peers)/2+1 && rf.log[rf.covertToArrIndex(N)].Term == rf.currentTerm {
				rf.commitIndex = N
				// rf.c.Broadcast()

				log.Printf("%d 的commitIndex=%d", rf.me, N)
				// if N >= index {
				// 	rf.applyCh <- ApplyMsg{
				// 		CommandValid: true,
				// 		Command:      command,
				// 		CommandIndex: index,
				// 	}
				// 	rf.mu.Unlock()
				// 	return
				// }
			}
		}
		if oldcommitIndex != rf.commitIndex {
			rf.c.Broadcast()
		}
		rf.mu.Unlock()

		time.Sleep(10 * time.Millisecond)
	}
}

//TODO:发送心跳
func (rf *Raft) sendAppendEntriesWithTicker() {
	for !rf.killed() {
		rf.mu.Lock()
		state := rf.state
		if state != Leader {
			rf.mu.Unlock()
			return
		}
		rf.mu.Unlock()
		rf.sendAppendEntriesToFollowers()
		time.Sleep(100 * time.Millisecond)
	}
}
func (rf *Raft) sendAppendEntriesToFollowers() {
	rf.mu.Lock()
	state := rf.state
	if state != Leader {
		rf.mu.Unlock()
		return
	}
	args := make([]AppendEntriesArgs, len(rf.peers))
	for i := 0; i < len(rf.peers); i++ {
		if arrIndex := rf.covertToArrIndex(rf.nextIndex[i] - 1); arrIndex < 0 {
			installSnapshotArgs := InstallSnapshotArgs{
				Term:              rf.currentTerm,
				LeaderId:          rf.me,
				LastIncludedIndex: rf.snapshotLastIncludedIndex,
				LastIncludedTerm:  rf.snapshotlastIncludedTerm,
				Data:              rf.snapshot,
			}
			go func(i int, copyArgs InstallSnapshotArgs) {
				args := &copyArgs
				reply := &InstallSnapshotReply{}
				ok := rf.sendInstallSnapshot(i, args, reply)
				if !ok {
					log.Printf("%d没有收到%d的安装快照回应", rf.me, i)
					return
				}
				rf.mu.Lock()
				defer rf.mu.Unlock()
				if reply.Term > rf.currentTerm {
					rf.currentTerm = reply.Term
					rf.state = Follower
					log.Printf("%d降级", rf.me)
					go rf.persistStateAndSnapshot()
					return
				}
				if copyArgs.Term != rf.currentTerm {
					log.Printf("%d收到过期安装快照回应from%d,args.term=%d,cur.term=%d\n", rf.me, i, args.Term, rf.currentTerm)
					return
				}
				if reply.Success {
					rf.nextIndex[i] = args.LastIncludedIndex + 1 //XXX
					rf.matchIndex[i] = args.LastIncludedTerm     //XXX
					log.Printf("%d为leader:rf.matchIndex[%d]:%v", rf.me, i, rf.matchIndex[i])
					log.Printf("%d为leader:rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])
					return
				}
				log.Printf("%d对%d发送的快照反应为:%v", i, rf.me, reply.Success)
			}(i, installSnapshotArgs)
			args[i] = AppendEntriesArgs{}
			continue
		}
		copyEntries := make([]Log, len(rf.log)-rf.covertToArrIndex(rf.nextIndex[i]))
		copy(copyEntries, rf.log[rf.covertToArrIndex(rf.nextIndex[i]):])
		args[i] = AppendEntriesArgs{
			Term:         rf.currentTerm,
			LeaderId:     rf.me,
			PrevLogIndex: rf.nextIndex[i] - 1,                                 //XXX
			PrevLogTerm:  rf.log[rf.covertToArrIndex(rf.nextIndex[i]-1)].Term, //XXX
			Entries:      copyEntries,
			LeaderCommit: rf.commitIndex, //XXX
		}
		log.Printf("%d正在准备向%d发送心跳,参数PrevLogIndex=%d", rf.me, i, args[i].PrevLogIndex)
	}

	for i := 0; i < len(rf.peers); i++ {
		if i == rf.me {
			// XXX是不是放这
			rf.nextIndex[rf.me] = rf.covertToVirtualIndex(len(rf.log))
			rf.matchIndex[rf.me] = rf.covertToVirtualIndex(len(rf.log) - 1)
			log.Printf("%d 跳过向 %d 发送心跳,参数PrevLogIndex = %d", rf.me, i, args[i].PrevLogIndex)
			continue
		}
		go func(i int, copy AppendEntriesArgs) {
			// rf.mu.Lock()
			// if i == rf.me {
			// 	rf.mu.Unlock()
			// 	return
			// }
			// args := &AppendEntriesArgs{
			// 	Term:         rf.currentTerm,
			// 	LeaderId:     rf.me,
			// 	PrevLogIndex: rf.nextIndex[i] - 1,            //XXX
			// 	PrevLogTerm:  rf.log[rf.nextIndex[i]-1].Term, //XXX
			// 	Entries:      rf.log[rf.nextIndex[i]:],
			// 	LeaderCommit: rf.commitIndex, //XXX
			// }
			// rf.mu.Unlock()
			// args := &AppendEntriesArgs{
			// 	Term:         copy.Term,
			// 	LeaderId:     copy.LeaderId,
			// 	PrevLogIndex: copy.PrevLogIndex,
			// 	PrevLogTerm:  copy.PrevLogTerm,
			// 	Entries:      copy.Entries,
			// 	LeaderCommit: copy.LeaderCommit,
			// }
			args := &copy
			if args.Term == 0 {
				log.Printf("%d没有向%d发心跳,发了快照", rf.me, i)
				return
			}
			reply := &AppendEntriesReply{}
			log.Printf("%d把从%d开始的log发送给%d", rf.me, args.PrevLogIndex+1, i)

			ok := rf.sendAppendEntries(i, args, reply)
			// TODO:检查reply
			if !ok {
				log.Printf("%d没有收到%d的心跳回应", rf.me, i)
				return
			}
			rf.mu.Lock()
			defer rf.mu.Unlock()
			log.Printf("%d对%d发送的心跳反应为:%v", i, rf.me, reply)

			if reply.Term > rf.currentTerm {
				rf.currentTerm = reply.Term
				rf.state = Follower
				log.Printf("%d降级", rf.me)
				go rf.persistStateAndSnapshot()
				return
			}
			if args.Term != rf.currentTerm {
				log.Printf("%d收到过期心跳回应from%d,args.term=%d,cur.term=%d\n", rf.me, i, args.Term, rf.currentTerm)
				return
			}
			// If successful: update nextIndex and matchIndex for follower
			if reply.Success {
				rf.nextIndex[i] = args.PrevLogIndex + len(args.Entries) + 1 //XXX
				rf.matchIndex[i] = args.PrevLogIndex + len(args.Entries)    //XXX
				log.Printf("%d为leader:rf.matchIndex[%d]:%v", rf.me, i, rf.matchIndex[i])
				return
			} else {
				// If AppendEntries fails because of log inconsistency: decrement nextIndex and retry
				// rf.nextIndex[i] = Max(args.PrevLogIndex-1, 1) //XXX
				if reply.XTerm < 0 {
					log.Printf("%d为leader:!原rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])

					rf.nextIndex[i] = Max(1, args.PrevLogIndex-reply.XLen+1)
					log.Printf("%d为leader:!rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])

				} else if rf.covertToArrIndex(reply.XIndex) <= 0 {
					rf.nextIndex[i] = reply.XIndex
					log.Printf("%d为leader:!!rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])
				} else if rf.log[rf.covertToArrIndex(reply.XIndex)].Term != reply.XTerm {
					rf.nextIndex[i] = reply.XIndex
					log.Printf("%d为leader:!!rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])

				} else {
					j := rf.covertToArrIndex(reply.XIndex + 1)
					for j < len(rf.log) && rf.log[j].Term == reply.XTerm {
						j++
					}
					rf.nextIndex[i] = rf.covertToVirtualIndex(j)
					log.Printf("%d为leader!!!:rf.nextIndex[%d]:%v", rf.me, i, rf.nextIndex[i])

				}
			}
		}(i, args[i])
	}
	rf.mu.Unlock()
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	log.Printf("%d正在被make", me)
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me

	// Your initialization code here (2A, 2B, 2C).
	rf = &Raft{
		mu:        sync.Mutex{},
		peers:     peers,
		persister: persister,
		me:        me,
		//初始化
		dead: 0, //?
		//TODO:怎么恢复
		currentTerm: 0,
		votedFor:    -1,
		log: []Log{{
			Command: "nil",
			Term:    0,
		}},
		snapshotLastIncludedIndex: 0,
		//非持久化存储
		commitIndex:          0,
		lastApplied:          0,
		nextIndex:            make([]int, len(peers)), //长度？
		matchIndex:           make([]int, len(peers)), //长度？
		ReceiveAppendEntries: make(chan int),
		GrantingVote:         make(chan int),
		VotePass:             make(chan int, len(peers)/2+1),
		applyCh:              applyCh,
	}
	rf.c = sync.NewCond(&rf.mu)
	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())
	rf.readPersistSnapshot(persister.ReadSnapshot())
	rf.lastApplied = rf.snapshotLastIncludedIndex
	rf.commitIndex = rf.snapshotLastIncludedIndex
	// <-rf.applyCh
	// rf.applyCh <- ApplyMsg{
	// 	CommandValid:  false,
	// 	Command:       nil,
	// 	CommandIndex:  0,
	// 	SnapshotValid: true,
	// 	Snapshot:      rf.snapshot,
	// 	SnapshotTerm:  rf.snapshotlastIncludedTerm,
	// 	SnapshotIndex: rf.snapshotLastIncludedIndex,
	// }
	// start ticker goroutine to start elections
	go rf.ticker()
	// go rf.applyChWithTicker()
	go rf.applyChWithCondition()
	return rf
}
