package common

import "time"

type TaskInfo struct {
	// Declared in consts above
	State int

	BeginTime time.Time
	FileName  string
	FileIndex int
	PartIndex int

	NReduce int
	NFiles  int //输入文件数量
}

const (
	MapState    = 0
	ReduceState = 1
	WaitState   = 2
	EndState    = 3
)
