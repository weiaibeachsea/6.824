package kvraft

import (
	"bytes"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"6.824/labgob"
	"6.824/labrpc"
	"6.824/raft"
)

const Debug = false

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug {
		log.Printf(format, a...)
	}
	return
}

const ExecuteTimeout = 300 * time.Millisecond

type Op struct {
	// Your definitions here.
	// Field names must start with capital letters,
	// otherwise RPC will break.
	// Command      interface{}
	Key          string
	Op           string
	Value        string
	CommandIndex int
	ReqID        int
	ClerkID      int64
}

type KVServer struct {
	mu      sync.Mutex
	me      int
	rf      *raft.Raft
	applyCh chan raft.ApplyMsg
	dead    int32 // set by Kill()

	maxraftstate int // snapshot if log grows this big

	// Your definitions here.
	data        map[string]string
	c           sync.Cond
	Transaction Op
	clerkRecord map[int64]RequestSignal
	notifyChan  map[int64]chan Op

	persister   *raft.Persister
	lastApplied int
}
type RequestSignal struct {
	ReqID int
	Value string
}

func (kv *KVServer) Get(args *GetArgs, reply *GetReply) {
	// Your code here.
	kv.mu.Lock()

	if kv.checkDuplicate(args.ReqID, args.ClerkID) {
		value := kv.clerkRecord[args.ClerkID].Value
		*reply = GetReply{
			Err:   OK,
			Value: value,
		}
		kv.mu.Unlock()
		return
	}
	kv.mu.Unlock()

	log.Printf("KVServer %d 发起 Start :%+v", kv.me, args)

	index, _, isLeader := kv.rf.Start(Op{
		Key: args.Key,
		Op:  "Get",
		// Value:        "",
		// CommandIndex: 0,
		ReqID:   args.ReqID,
		ClerkID: args.ClerkID,
	})
	if !isLeader {
		*reply = GetReply{
			Err: ErrWrongLeader,
		}
		return
	}
	// log.Printf("Get index = %d", index)
	log.Printf("KVServer %d :Get index = %d, args = %+v", kv.me, index, args)

	// kv.c.L.Lock()
	// defer kv.c.L.Unlock()

	// for kv.Transaction.CommandIndex < index {
	// 	kv.c.Wait()
	// }
	// if kv.Op.CommandIndex > index {
	// 	return
	// }
	ch := kv.makeNotifyChan(int64(index))
	// select {
	// case <-ch:
	// 	{

	// 	}
	// }
	var op Op
	select {
	case op = <-ch:
		{
			log.Printf("OpCommandIndex==index:%v,expectindex=%d,OpCommandIndex=%d", op.CommandIndex == index, index, op.CommandIndex)

			if op.Key != args.Key || op.Op != "Get" {
				*reply = GetReply{
					Err: ErrWrongLeader,
				}
				log.Printf("%d似乎失去了领导地位 when args = %+v, but result = %+v", kv.me, args, op)
				// kv.c.L.Unlock()
				return
			}
			*reply = GetReply{
				Err:   OK,
				Value: op.Value,
			}
		}
	case <-time.After(ExecuteTimeout):
		{
			*reply = GetReply{
				Err: ErrTimeOut,
			}
			log.Printf("%d 处理 Get 任务超时 when args = %+v , index = %d ", kv.me, args, index)
			// kv.c.L.Unlock()
			return
		}
	}
	// log.Printf("OpCommandIndex==index:%v,expectindex=%d,OpCommandIndex=%d", op.CommandIndex == index, index, op.CommandIndex)

	// *reply = GetReply{
	// 	Err:   OK,
	// 	Value: op.Value,
	// }
	// if op.Value == "" {
	// 	reply.Err = ErrNoKey
	// }
	// close(ch)
}

func (kv *KVServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	// Your code here.
	// kv.c.L.Lock()
	// defer kv.c.L.Unlock()

	// ReqID, ok := kv.clerkRecord[args.ClerkID]
	// if ok {
	// 	if args.ReqID <= ReqID {
	// 		kv.c.L.Unlock()
	// 		*reply = PutAppendReply{
	// 			Err: OK,
	// 		}
	// 		return
	// 	}
	// }
	kv.mu.Lock()

	if kv.checkDuplicate(args.ReqID, args.ClerkID) {
		*reply = PutAppendReply{
			Err: OK,
		}
		kv.mu.Unlock()
		return
	}
	kv.mu.Unlock()

	// kv.c.L.Unlock()
	log.Printf("KVServer %d 发起 Start :%+v", kv.me, args)
	index, _, isLeader := kv.rf.Start(Op{
		Key:   args.Key,
		Op:    args.Op,
		Value: args.Value,
		// CommandIndex: 0,
		ReqID:   args.ReqID,
		ClerkID: args.ClerkID,
	})
	if !isLeader {
		*reply = PutAppendReply{
			Err: ErrWrongLeader,
		}
		return
	}
	log.Printf("KVServer %d :PutAppend index = %d, args = %+v", kv.me, index, args)

	ch := kv.makeNotifyChan(int64(index))
	// var op Op
	select {
	case op := <-ch:
		{
			log.Printf("OpCommandIndex==index:%v,expectindex=%d,OpCommandIndex=%d", op.CommandIndex == index, index, op.CommandIndex)

			if op.Key != args.Key || op.Op != args.Op || op.Value != args.Value {
				*reply = PutAppendReply{
					Err: ErrWrongLeader,
				}
				log.Printf("%d似乎失去了领导地位 when args = %+v, but result = %+v", kv.me, args, op)
				// kv.c.L.Unlock()
				return
			}
			*reply = PutAppendReply{
				Err: OK,
			}
		}
	case <-time.After(ExecuteTimeout):
		{
			*reply = PutAppendReply{
				Err: ErrTimeOut,
			}
			log.Printf("%d 处理PutAppend任务超时 when args = %+v , index = %d ", kv.me, args, index)
			// kv.c.L.Unlock()
			// return
		}
	}
	// kv.c.L.Lock()

	//XXX TESt
	// *reply = PutAppendReply{
	// 	Err: OK,
	// }
	// kv.c.L.Unlock()
	// return
	// kv.c.L.Lock()

	// for kv.Transaction.CommandIndex < index {
	// 	kv.c.Wait()
	// }
	// if kv.Op.CommandIndex > index {
	// 	return
	// }

	// kv.mu.Lock()
	// kv.clerkRecord[args.ClerkID] = RequestSignal{
	// 	ReqID: args.ReqID,
	// 	Value: "", //XXX???
	// }

	// *reply = PutAppendReply{
	// 	Err: OK,
	// }
	// kv.c.L.Unlock()

}
func (kv *KVServer) makeNotifyChan(index int64) chan Op {
	channel := make(chan Op, 1)
	kv.mu.Lock()
	defer kv.mu.Unlock()
	kv.notifyChan[index] = channel
	return channel
}

func (kv *KVServer) getNotifyChan(index int64) chan Op {
	// kv.mu.Lock()
	// defer kv.mu.Unlock()
	channel := kv.notifyChan[index]
	return channel
}

func (kv *KVServer) getAppliedMsg() {
	for m := range kv.applyCh {

		if m.CommandValid {
			kv.mu.Lock()

			op := Op{
				Key:          m.Command.(Op).Key,
				Op:           m.Command.(Op).Op,
				Value:        "",
				CommandIndex: m.CommandIndex,
			}

			if op.Op != "Get" && kv.checkDuplicate(m.Command.(Op).ReqID, m.Command.(Op).ClerkID) {
				op.Value = kv.clerkRecord[op.ClerkID].Value
				log.Printf("%d 检测到重复, 不执行: %+v", kv.me, m.Command.(Op))
			} else {
				switch m.Command.(Op).Op {
				case "Get":
					{
						// kv.Transaction.Value = kv.data[m.Command.(Op).Key]
						op.Value = kv.data[m.Command.(Op).Key]
					}
				case "Put":
					{
						kv.data[m.Command.(Op).Key] = m.Command.(Op).Value
						// kv.Transaction.Value = m.Command.(Op).Value
						op.Value = m.Command.(Op).Value
					}
				case "Append":
					{
						kv.data[m.Command.(Op).Key] = kv.data[m.Command.(Op).Key] + m.Command.(Op).Value
						// kv.Transaction.Value = m.Command.(Op).Value
						op.Value = m.Command.(Op).Value
					}
				}
				// kv.Transaction = Op{
				// 	Key:          m.Command.(Op).Key,
				// 	Op:           m.Command.(Op).Op,
				// 	Value:        kv.data[m.Command.(Op).Key],
				// 	CommandIndex: m.CommandIndex,
				// }
				log.Printf("%d 取出 msg : %+v", kv.me, m)
				log.Printf("%d op : %+v", kv.me, op)
				log.Printf("KVServer %d 应用了序号 = %d", kv.me, m.CommandIndex)

				kv.clerkRecord[m.Command.(Op).ClerkID] = RequestSignal{
					ReqID: m.Command.(Op).ReqID,
					Value: op.Value,
				}
			}
			kv.lastApplied = m.CommandIndex

			ch := kv.getNotifyChan(int64(m.CommandIndex))
			if ch != nil {
				log.Printf("%d 正在令 %d hanlder 返回", kv.me, m.CommandIndex)
				ch <- op
				log.Printf("%d 令 %d hanlder 返回", kv.me, m.CommandIndex)
				close(ch)
				delete(kv.notifyChan, int64(m.CommandIndex))
				//BUG 由于快照，可能导致多次写通道，不能这么写
			}
			kv.mu.Unlock()

		} else {
			kv.readPersistSnapshot(m.Snapshot)
			// kv.lastApplied = m.SnapshotIndex
		}
		// kv.c.Broadcast()
	}
}
func (kv *KVServer) checkDuplicate(ReqID int, ClerkID int64) bool {
	// kv.mu.Lock()
	// defer kv.mu.Unlock()
	return kv.clerkRecord[ClerkID].ReqID >= ReqID
}

func (kv *KVServer) checkLogSize() {
	for !kv.killed() {
		if kv.maxraftstate <= kv.persister.RaftStateSize() {
			w := new(bytes.Buffer)
			e := labgob.NewEncoder(w)
			kv.mu.Lock()
			e.Encode(kv.data)
			e.Encode(kv.clerkRecord)
			e.Encode(kv.lastApplied)
			// e.Encode(rf.snapshotLastIncludedIndex)

			// Buffer := new(bytes.Buffer)
			// LabEncoder := labgob.NewEncoder(Buffer)
			// LabEncoder.Encode(rf.snapshotLastIncludedIndex)
			// LabEncoder.Encode(rf.snapshot)
			// LabEncoder.Encode(rf.snapshotlastIncludedTerm)

			data := w.Bytes()
			kv.rf.Snapshot(kv.lastApplied, data)
			kv.mu.Unlock()

		}
		time.Sleep(100 * time.Millisecond)
	}
}

//
// the tester calls Kill() when a KVServer instance won't
// be needed again. for your convenience, we supply
// code to set rf.dead (without needing a lock),
// and a killed() method to test rf.dead in
// long-running loops. you can also add your own
// code to Kill(). you're not required to do anything
// about this, but it may be convenient (for example)
// to suppress debug output from a Kill()ed instance.
//
func (kv *KVServer) Kill() {
	atomic.StoreInt32(&kv.dead, 1)
	kv.rf.Kill()
	// Your code here, if desired.
	log.Printf("KVServer %d 死了", kv.me)
}

func (kv *KVServer) killed() bool {
	z := atomic.LoadInt32(&kv.dead)
	return z == 1
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Raft to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
// the k/v server should store snapshots through the underlying Raft
// implementation, which should call persister.SaveStateAndSnapshot() to
// atomically save the Raft state along with the snapshot.
// the k/v server should snapshot when Raft's saved state exceeds maxraftstate bytes,
// in order to allow Raft to garbage-collect its log. if maxraftstate is -1,
// you don't need to snapshot.
// StartKVServer() must return quickly, so it should start goroutines
// for any long-running work.
//
func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *KVServer {
	// call labgob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	labgob.Register(Op{})

	kv := new(KVServer)
	kv.me = me
	kv.maxraftstate = maxraftstate

	// You may need initialization code here.

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)

	// You may need initialization code here.
	kv.c = sync.Cond{
		L: &kv.mu,
	}
	kv.data = make(map[string]string)
	kv.clerkRecord = make(map[int64]RequestSignal)
	kv.notifyChan = make(map[int64]chan Op)
	kv.persister = persister
	// rf.readPersist(persister.ReadRaftState())
	kv.readPersistSnapshot(persister.ReadSnapshot())
	go kv.getAppliedMsg()
	if maxraftstate != -1 {
		go kv.checkLogSize()
	}
	return kv
}

func (kv *KVServer) readPersistSnapshot(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}
	kv.mu.Lock()
	defer kv.mu.Unlock()
	r := bytes.NewBuffer(data)
	d := labgob.NewDecoder(r)
	// var snapshot []byte
	// var snapshotLastIncludedIndex int
	// var snapshotlastIncludedTerm int
	// // var logArr []Log
	var kvPair map[string]string
	var clerkRecord map[int64]RequestSignal
	var lastApplied int
	if d.Decode(&kvPair) != nil ||
		d.Decode(&clerkRecord) != nil ||

		d.Decode(&lastApplied) != nil {
		// 	//   error...
		log.Panicf("Decode失败:%+v", d.Decode(&kv))
	} else {
		// rf.snapshot = snapshot
		// rf.snapshotLastIncludedIndex = snapshotLastIncludedIndex
		// rf.snapshotlastIncludedTerm = snapshotlastIncludedTerm
		// rf.log = logArr
		kv.data = kvPair
		kv.clerkRecord = clerkRecord
		kv.lastApplied = lastApplied
		log.Printf("KVServer %d 应用了快照: snapshot.lastApplied = %d", kv.me, kv.lastApplied)
	}
	// rf.snapshot = data

}
