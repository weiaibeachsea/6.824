package kvraft

import (
	"crypto/rand"
	"log"
	"math/big"
	"sync"

	"6.824/labrpc"
)

type Clerk struct {
	servers []*labrpc.ClientEnd
	// You will have to modify this struct.
	me       int64
	reqID    int
	mu       sync.Mutex
	LeaderID int
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	// You'll have to add code here.
	ck.me = nrand()
	return ck
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) Get(key string) string {

	// You will have to modify this function.
	log.Printf("看看一秒调用多少次")
	ck.mu.Lock()
	ck.reqID++
	reqID := ck.reqID
	leaderID := ck.LeaderID
	ck.mu.Unlock()
	args := &GetArgs{
		Key:     key,
		ReqID:   reqID,
		ClerkID: ck.me,
	}
	reply := &GetReply{}
	i := leaderID
	for {
		for ; i < len(ck.servers); i++ {
			log.Printf("Clerk 向 %d 发起 Get :key=%s", i, key)
			ok := ck.servers[i].Call("KVServer.Get", args, reply)
			if ok {
				if reply.Err == OK {
					log.Printf("getKey success [%s]=%s", key, reply.Value)
					ck.mu.Lock()
					ck.LeaderID = i
					ck.mu.Unlock()
					return reply.Value
					// break //XXX如果都失败了？
				} else if reply.Err == ErrWrongLeader {
					log.Printf("server %d:"+ErrWrongLeader, i)
					continue
				} else if reply.Err == ErrTimeOut {
					log.Printf("server %d:"+ErrTimeOut, i)
					continue
				} else {
					log.Printf("server %d:"+ErrNoKey, i)
					return ""
				}
			} else {
				log.Printf("Clerk %d Get failure 没有收到来自 %d 回应:", ck.me, i)
				continue
			}
		}
		i = 0
	}

	return ""
}

//
// shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string) {
	// You will have to modify this function.
	ck.mu.Lock()
	ck.reqID++
	reqID := ck.reqID
	leaderID := ck.LeaderID
	ck.mu.Unlock()
	args := &PutAppendArgs{
		Key:     key,
		Value:   value,
		Op:      op,
		ReqID:   reqID,
		ClerkID: ck.me,
	}
	reply := &PutAppendReply{}
	i := leaderID
	for {
		for ; i < len(ck.servers); i++ {
			log.Printf("Clerk 向 %d 发起 PutAppend :key=%s,value=%s,op=%s", i, key, value, op)
			ok := ck.servers[i].Call("KVServer.PutAppend", args, reply)
			if ok {
				if reply.Err == OK {
					log.Printf("PutAppend success ,Key=%s", key)
					ck.mu.Lock()
					ck.LeaderID = i
					ck.mu.Unlock()
					return //XXX如果都失败了？
				} else if reply.Err == ErrWrongLeader {
					log.Printf("server %d:"+ErrWrongLeader, i)
					continue
				} else if reply.Err == ErrTimeOut {
					log.Printf("server %d:"+ErrTimeOut, i)
					continue
				}
			} else {
				log.Printf("Clerk %d PutAppend failure 没有收到来自 %d 回应:", ck.me, i)
				continue
			}
		}
		i = 0
	}

}

func (ck *Clerk) Put(key string, value string) {
	log.Printf("看看一秒调用多少次")
	ck.PutAppend(key, value, "Put")
}
func (ck *Clerk) Append(key string, value string) {
	log.Printf("看看一秒调用多少次")
	ck.PutAppend(key, value, "Append")
}

// func (ck *Clerk) sendStart(server int, command interface{}) bool {
// 	ok := ck.servers[server].Call("Raft.Start", command, nil)
// 	return ok
// }
